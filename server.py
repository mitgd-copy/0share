# Copyright (C) 2008, Thomas Leonard
# See the README file for details, or visit http://0install.net.

from logging import info, debug
import socket
import select, os
from BaseHTTPServer import HTTPServer, BaseHTTPRequestHandler

from zeroinstall.zerostore import Stores, BadDigest, NotStored

stores = Stores()

def serve(host, port):
	"""Listen on (host, port) for broadcast queries and fetch requests."""

	# The UDP socket listens for broadcast queries about who has
	# particular digests
	udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, 0)
	udp_socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
	udp_socket.bind((host or '<broadcast>', port))

	# The TCP socket is used to send the requested implementations
	http_server = HTTPServer((host or '', port), FetchRequestHandler)

	info("0share started and listening for requests...")
	for x in stores.stores:
		info("Serving implementations from %s", x)
	while True:
		ready = select.select([udp_socket, http_server.socket], [], [])[0]
		if udp_socket in ready:
			handle_query(udp_socket)
		if http_server.socket in ready:
			http_server.handle_request()

def handle_query(ss):
	data, addr = ss.recvfrom(128)
	info("Request from %s: %s", addr, repr(data))
	if not data.startswith('0share\n'):
		info("Not our format. Ignoring.")
		return
	digests = data.split('\n')[1:]
	got = []
	try:
		for d in digests:
			if not d: raise BadDigest("Empty digest!")
			try:
				stores.lookup(d)
				info("Yes, we have %s", d)
				got.append(d)
			except NotStored:
				info("No, we don't have %s", d)
	except BadDigest, ex:
		info("Bad request: %s", str(ex))
		return
	if got:
		info("Sending reply...")
		ss.sendto('0share-reply\n' + '\n'.join(got), addr)

class FetchRequestHandler(BaseHTTPRequestHandler):
	def do_GET(self):
		info("GET %s", self.path)
		bits = self.path.split('/')
		if len(bits) == 3 and bits[0] == '' and bits[1] == 'implementation':
			digest = bits[2]
		else:
			self.send_error(404, "Not an implementation")
			return

		try:
			path = stores.lookup(digest)
		except (BadDigest, NotStored), ex:
			info("Lookup error: %s", str(ex))
			self.send_error(404, "Implementation %s not found" % digest)
			return

		self.send_response(200, "OK")
		self.send_header('Content-Type', 'application/x-compressed-tar')
		self.end_headers()

		import tarfile
		archive = tarfile.open(mode = 'w|gz', fileobj = self.wfile)
		for x in os.listdir(path):
			if x != '.manifest':
				debug("Adding %s", x)
				archive.add(os.path.join(path, x), x)
		archive.close()
